package disk

import (
	"log"
	"os"
	"path/filepath"
	"syscall"

	"github.com/alecthomas/units"
)

// FreeSpace queries free space at the given path
func FreeSpace(dir string) (units.Base2Bytes, error) {
	var stat syscall.Statfs_t
	var err error

	if dir == "" {
		dir, err = os.Getwd()
		if err != nil {
			log.Printf("Cannot find working dir: %s", err)
			return 0, err
		}
	} else {
		dir, err = filepath.Abs(dir)
		if err != nil {
			log.Printf("Cannot resolve path '%s': %s", dir, err)
			return 0, err
		}
	}
	err = syscall.Statfs(dir, &stat)
	if err != nil {
		log.Printf("Cannot stat '%s': %s", dir, err)
		return 0, err
	}
	free := stat.Bavail * uint64(stat.Bsize)

	return units.Base2Bytes(free), nil
}

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/adhoc-gti/status_monitor/terminal"
)

type Test struct {
	Target   string
	Type     string
	Delay    int
	Insecure bool
}

type Result struct {
	Success  bool
	Warning  bool
	Disabled bool
	Code     int
	Test     Test
}

func (r Result) Status() string {
	if r.Success {
		return "up"
	} else {
		return "down"
	}
}

func main() {
	var endpoint string

	if len(os.Args) > 1 {
		endpoint = fmt.Sprintf("http://%s/api", os.Args[1])
	} else {
		fmt.Fprintf(os.Stderr, "error: missing server name")
		os.Exit(64)
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for _ = range c {
			terminal.Unalt()
			os.Exit(0)
		}
	}()

	terminal.Alt()
	terminal.ClearScreen()
	defer func() {
		if r := recover(); r != nil {
			terminal.Unalt()
			panic(r)
		}
	}()

	resultsURI := fmt.Sprintf("%s/results", endpoint)

	hadErrors := false

	for {
		func() {
			res, err := http.Get(resultsURI)
			if err != nil {
				fmt.Fprintf(os.Stdout, "\r\033[0;41;1;37m\033[K")
				fmt.Fprintf(os.Stdout, "Error: %s @%s\n", err, time.Now().Format("2006-01-02 15:04:05"))

				time.Sleep(time.Duration(15) * time.Second)
				return
			}
			defer res.Body.Close()

			if res.StatusCode != 200 {
				fmt.Fprintf(os.Stdout, "\r\033[0;41;1;37m\033[K")
				fmt.Fprintf(os.Stdout, "Error: %s @%s\n", res.Status, time.Now().Format("2006-01-02 15:04:05"))

				time.Sleep(time.Duration(15) * time.Second)
				return
			}

			body, err := ioutil.ReadAll(res.Body)
			if err != nil {
				fmt.Fprintf(os.Stdout, "\r\033[0;41;1;37m\033[K")
				fmt.Fprintf(os.Stdout, "Error: %s @%s\n", err, time.Now().Format("2006-01-02 15:04:05"))

				time.Sleep(time.Duration(15) * time.Second)
				return
			}

			results := make([]Result, 0)
			err = json.Unmarshal(body, &results)
			if err != nil {
				fmt.Fprintf(os.Stdout, "\r\033[0;41;1;37m\033[K")
				fmt.Fprintf(os.Stdout, "Error: %s @%s\n", err, time.Now().Format("2006-01-02 15:04:05"))

				time.Sleep(time.Duration(15) * time.Second)
				return
			}

			rowFmt := "%8s %8s %8s %s"

			fmt.Fprintf(os.Stdout, "\033[0;0H")
			fmt.Fprintf(os.Stdout, "\033[44;1;37m\033[K")
			fmt.Fprintf(os.Stdout, rowFmt,
				"STATUS",
				"TYPE",
				"CODE",
				"TARGET")
			fmt.Fprintf(os.Stdout, "\n")

			hasErrors := false
			for _, result := range results {
				hasErrors = hasErrors || !result.Success
			}
			if !hadErrors && hasErrors {
				fmt.Fprintf(os.Stdout, "\a")
			}
			hadErrors = hasErrors

			c := terminal.Cols()
			l := terminal.Lines() - 2
			if l < len(results) {
				results = results[:l]
			}

			for _, result := range results {
				if result.Disabled {
					fmt.Fprintf(os.Stdout, "\033[0;47;1;30m\033[K")
				} else if result.Warning {
					fmt.Fprintf(os.Stdout, "\033[0;43;1;37m\033[K")
				} else if result.Success {
					fmt.Fprintf(os.Stdout, "\033[0;42;1;37m\033[K")
				} else {
					fmt.Fprintf(os.Stdout, "\033[0;41;1;37m\033[K")
				}
				row := fmt.Sprintf(rowFmt,
					result.Status(),
					result.Test.Type,
					fmt.Sprintf("%d", result.Code),
					result.Test.Target)
				if c < len(row) {
					row = row[:c]
				}
				fmt.Fprintf(os.Stdout, row)
				fmt.Fprintf(os.Stdout, "\n")
			}
			fmt.Fprintf(os.Stdout, "\033[44;1;37m\033[K")
			fmt.Fprintf(os.Stdout, "Updated: %s", time.Now().Format("2006-01-02 15:04:05"))

			time.Sleep(time.Duration(1) * time.Second)
		}()
	}
}

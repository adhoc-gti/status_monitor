# Status Monitor

Lightweight monitoring application scheduling, running, aggregating and
smoothing test results, sending emails on significant changes, along with
its terminal display client.

module gitlab.com/adhoc-gti/status_monitor

require (
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf
	github.com/gorilla/context v0.0.0-20160226214623-1ea25387ff6f
	github.com/gorilla/mux v0.0.0-20170524010104-043ee6597c29
	github.com/urfave/negroni v0.0.0-20170506022741-1d8981a52d6b
	golang.org/x/net v0.0.0-20170529214944-3da985ce5951
	golang.org/x/text v0.0.0-20170512150324-19e51611da83
	gopkg.in/yaml.v2 v2.0.0-20170407172122-cd8b52f8269e
)

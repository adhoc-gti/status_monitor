package scheduler

import (
	"math/rand"
	"time"

	"gitlab.com/adhoc-gti/status_monitor/server/test"
)

// ScheduleTests queues each given test for execution
func ScheduleTests(tests []test.Test, queue chan<- test.Test) {
	for _, t := range tests {
		// reschedule test
		go func(t test.Test) {
			fuzz := time.Duration(rand.Float64() * float64(len(tests)) * float64(time.Second))
			time.Sleep(fuzz)
			if t.Active(time.Now()) {
				queue <- t
			}

			for {
				delay := time.Duration(float64(t.Delay) * float64(time.Second))
				offset := (rand.Float64() - 0.5) * float64(t.Delay)
				fuzz := time.Duration(offset * float64(time.Second))
				time.Sleep(delay + fuzz)
				if t.Active(time.Now()) {
					queue <- t
				}
			}
		}(t)
	}
}

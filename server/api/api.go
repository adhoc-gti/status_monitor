package api

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"sort"

	"gitlab.com/adhoc-gti/status_monitor/server/status"
	"gitlab.com/adhoc-gti/status_monitor/server/test"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// Serve responds to HTTP requests
func Serve(status *status.Status) {
	router := mux.NewRouter()
	router.StrictSlash(false)

	api := router.PathPrefix("/api/").Subrouter()
	api.Path("/results").Methods("GET").HandlerFunc(func(rw http.ResponseWriter, r *http.Request) { ListResults(status, rw, r) })

	n := negroni.Classic()
	n.UseHandler(router)
	n.Run(":8000")
}

// ByFailure sorts test results failure first
type ByFailure []test.TestResult

func (a ByFailure) Len() int      { return len(a) }
func (a ByFailure) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByFailure) Less(i, j int) bool {
	if a[i].Success {
		return false
	}

	return a[j].Success
}

// ByTarget sorts test results alphabetically by test target
type ByTarget []test.TestResult

func (a ByTarget) Len() int      { return len(a) }
func (a ByTarget) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByTarget) Less(i, j int) bool {
	return bytes.Compare([]byte(a[i].Test.Target), []byte(a[j].Test.Target)) < 0
}

// ListResults renders results as a nice JSON list
func ListResults(status *status.Status, rw http.ResponseWriter, r *http.Request) {
	rw.Header().Add("Content-Type", "application/json")

	results := status.Results()
	sort.Sort(ByTarget(results))
	sort.Stable(ByFailure(results))

	response, err := json.Marshal(results)
	if err != nil {
		log.Fatal(err)
	}

	_, err = rw.Write(response)
	if err != nil {
		log.Fatal(err)
	}
}

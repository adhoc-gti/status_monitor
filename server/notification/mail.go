package notification

import (
	"encoding/base64"
	"fmt"
	"log"
	"mime"
	"net/smtp"

	"gitlab.com/adhoc-gti/status_monitor/server/config"
)

// SendMail
func SendMailNotification(subject string, body string) {
	from := config.Mail().FromAddress()
	to := config.Mail().ToAddress()

	header := make(map[string]string)
	header["From"] = from.String()
	header["To"] = to.String()
	header["Subject"] = mime.QEncoding.Encode("utf-8", subject)
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/plain; charset=\"utf-8\""
	header["Content-Transfer-Encoding"] = "base64"

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(body))

	addr := config.Mail().SMTP

	err := smtp.SendMail(addr, nil, from.Address, []string{to.Address}, []byte(message))
	if err != nil {
		log.Println(err)
	}
}

package notification

import (
	"bytes"
	"fmt"
	"time"

	"gitlab.com/adhoc-gti/status_monitor/server/test"
)

// SendUpNotification
func SendUpNotification(at time.Time, result test.TestResult) {
	var body bytes.Buffer
	body.WriteString(fmt.Sprintf("Code: %d\n", result.Code))
	body.WriteString(fmt.Sprintf("Emitted: %s\n", at.Format("2 Jan 2006 15:04:05 MST")))
	SendMailNotification(fmt.Sprintf("[UP] %s", result.Test.Target), body.String())
}

// SendDownNotification
func SendDownNotification(at time.Time, result test.TestResult) {
	var body bytes.Buffer
	body.WriteString(fmt.Sprintf("Code: %d\n", result.Code))
	body.WriteString(fmt.Sprintf("Emitted: %s\n", at.Format("2 Jan 2006 15:04:05 MST")))
	SendMailNotification(fmt.Sprintf("[DOWN] %s", result.Test.Target), body.String())
}

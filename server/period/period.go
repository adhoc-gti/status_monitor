package period

import (
	"fmt"
	"strconv"
	"time"
)

// Period desribes a periodic range of time
type Period struct {
	Start string `json:"start"`
	End   string `json:"end"`
	// TODO: allow for day of week occurence
}

// StartTime computes the start time of a periodic range
func (p Period) StartTime() (start time.Time) {
	// TODO: allow for fixed date+time
	// TODO: allow for timezone
	today := time.Now().UTC().Format("2006-02-01")
	str := fmt.Sprintf("%s %s", today, p.Start)
	start, _ = time.Parse("2006-02-01 15:04:05", str)
	if start.After(time.Now()) {
		start = start.AddDate(0, 0, -1)
	}
	return start
}

// EndTime computes the end time of a periodic range
func (p Period) EndTime() (end time.Time) {
	// TODO: allow for absolute time
	// TODO: allow for fixed date+time
	// TODO: allow for timezone
	if len(p.End) > 1 && p.End[0] == '+' {
		// TODO: use time.ParseDuration
		start := p.StartTime()
		duration, err := strconv.ParseInt(p.End[1:len(p.End)], 0, 0)
		if err != nil {
			return end
		}
		end = start.Add(time.Duration(duration) * time.Second)
	}
	return end
}

// Current checks if the periodic range is current to the given time
func (p Period) Current(t time.Time) bool {
	return t.After(p.StartTime()) && t.Before(p.EndTime())
}

package test

import (
	"os/exec"
	"syscall"
)

// Ping implements a ping test
func Ping(test Test) TestResult {
	target := test.Target
	cmd := exec.Command("ping", "-c1", target)
	err := cmd.Run()
	if err != nil {
		rc := 1
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				rc = status.ExitStatus()
			}
		}
		return TestResult{Success: false, Test: test, Code: rc}
	}

	return TestResult{Success: true, Test: test, Code: 0}
}

// Ping6 implements a ping test
func Ping6(test Test) TestResult {
	target := test.Target
	cmd := exec.Command("ping6", "-c1", target)
	err := cmd.Run()
	if err != nil {
		rc := 1
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				rc = status.ExitStatus()
			}
		}
		return TestResult{Success: false, Test: test, Code: rc}
	}

	return TestResult{Success: true, Test: test, Code: 0}
}

package test

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/adhoc-gti/status_monitor/server/period"
)

// Test describes a test to be performed
type Test struct {
	Target   string        `json:"target"`
	Type     string        `json:"type"`
	Delay    int           `json:"delay"`
	Insecure bool          `json:"insecure"`
	Off      period.Period `json:"off"`
}

// Active checks if the test is active at the given instant in time
func (t Test) Active(instant time.Time) bool {
	return !t.Off.Current(instant)
}

// Run executes a test and returns the result
func (t Test) Run() (result TestResult) {
	switch t.Type {
	case "ping":
		result = Ping(t)
	case "ping6":
		result = Ping6(t)
	case "http":
		result = HTTP(t)
	default:
		log.Fatal(fmt.Sprintf("unknown type %s", t.Type))
	}

	return result
}

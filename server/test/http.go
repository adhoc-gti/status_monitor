package test

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"sync"

	"gitlab.com/adhoc-gti/status_monitor/server/config"

	"golang.org/x/net/publicsuffix"
)

// CheckRedirectWithHeaders sets headers again when following redirects
func CheckRedirectWithHeaders(req *http.Request, via []*http.Request) error {
	if len(via) >= 10 {
		return fmt.Errorf("too many redirects")
	}
	if len(via) == 0 {
		return nil
	}
	for attr, val := range via[0].Header {
		if _, ok := req.Header[attr]; !ok {
			req.Header[attr] = val
		}
	}
	return nil
}

// HTTPTestClients provides a concurrency-safe test->client map
type HTTPTestClients struct {
	clients map[Test]*http.Client
	mu      sync.RWMutex
}

// Get retrieves or inits http.Client as needed for a test
func (h *HTTPTestClients) Get(test Test) (client *http.Client) {
	h.mu.RLock()
	if c, ok := h.clients[test]; ok {
		client = c
		h.mu.RUnlock()
	} else {
		h.mu.RUnlock()
		options := cookiejar.Options{
			PublicSuffixList: publicsuffix.List,
		}
		jar, err := cookiejar.New(&options)
		if err != nil {
			log.Fatal(err)
		}
		if test.Insecure {
			tr := &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			}
			client = &http.Client{Jar: jar, Transport: tr}
		} else {
			client = &http.Client{Jar: jar}
		}
		h.mu.Lock()
		h.clients[test] = client
		h.mu.Unlock()
	}

	return
}

var httpTestClients = HTTPTestClients{clients: map[Test]*http.Client{}}

// HTTP implements a HTTP test
func HTTP(test Test) TestResult {
	client := httpTestClients.Get(test)

	client.CheckRedirect = CheckRedirectWithHeaders
	target := test.Target

	req, err := http.NewRequest("GET", target, nil)
	if err != nil {
		return TestResult{Success: false, Test: test, Code: 0}
	}

	req.Header.Set("User-Agent", "StatusBot/1.0")
	for header, value := range config.HTTP().Headers {
		req.Header.Set(header, value)
	}

	res, err := client.Do(req)
	if err != nil {
		return TestResult{Success: false, Test: test, Code: 0}
	}
	defer res.Body.Close()

	result := TestResult{Test: test, Code: res.StatusCode}

	validate := func(code int) bool {
		return code < 300 || code == 401
	}

	warn := func(code int) bool {
		return code == 417
	}

	_, err = ioutil.ReadAll(res.Body)
	if err != nil {
		return result
	}

	result.Success = validate(res.StatusCode)
	result.Warning = warn(res.StatusCode)

	return result
}

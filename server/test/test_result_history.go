package test

import "sync"

// TestResultHistory records TestTesult values over time
type TestResultHistory struct {
	results []TestResult
	last    int
	size    int
	mu      sync.RWMutex
}

// NewTestResultHistory creates a new history buffer of the given size
func NewTestResultHistory(size int) *TestResultHistory {
	r := make([]TestResult, size)
	p := &TestResultHistory{results: r, size: size}
	return p
}

// Append a test to the history buffer
func (h *TestResultHistory) Append(r TestResult) {
	h.mu.Lock()
	defer h.mu.Unlock()

	if h.size == 0 {
		panic("uninitialized buffer")
	}

	next := h.last - 1
	if next < 0 {
		next = len(h.results) - 1
	}
	h.results[next] = r
	h.last = next
}

// Last is the last test result
func (h *TestResultHistory) Last() TestResult {
	h.mu.RLock()
	defer h.mu.RUnlock()

	return h.results[h.last]
}

// Success evaluates success trend
func (h *TestResultHistory) Success() bool {
	h.mu.RLock()
	defer h.mu.RUnlock()

	count := 0
	for _, result := range h.results {
		if result.Success || result.Test.Type == "" {
			count++
		}
	}
	return count > h.size/2
}

// Warning evaluates warning trend
func (h *TestResultHistory) Warning() bool {
	h.mu.RLock()
	defer h.mu.RUnlock()

	count := 0
	for _, result := range h.results {
		if result.Warning || result.Test.Type == "" {
			count++
		}
	}
	return count > h.size/2
}

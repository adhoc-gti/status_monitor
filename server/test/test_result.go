package test

// TestResult stores data about a test's observed behavior
type TestResult struct {
	Success  bool `json:"success"`
	Warning  bool `json:"warning"`
	Disabled bool `json:"disabled"`
	Test     Test `json:"test"`
	Code     int  `json:"code"`
}

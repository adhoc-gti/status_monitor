require 'sinatra'

set :bind, '127.0.0.1'

before do
  %w(HTTP_X_WHATEVER).each do |env|
    puts "#{env} = #{request.env[env].inspect}"
  end
end

get '/' do
  'Hello World!'
end

get '/warn' do
  status 417
  'Warning World!'
end

get '/nope' do
  status 404
  'Nope World!'
end

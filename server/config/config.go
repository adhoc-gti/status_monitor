package config

import (
	"io/ioutil"
	"log"
	"net/mail"

	yaml "gopkg.in/yaml.v2"
)

// MailConfig contains elements of configuration
type MailConfig struct {
	SMTP string `json:"smtp"`
	From string `json:"from"`
	To   string `json:"to"`
}

// FromAddress returns a parsed address to send form
func (c MailConfig) FromAddress() *mail.Address {
	m, _ := mail.ParseAddress(c.From)
	return m
}

// ToAddress returns a parsed address to send to
func (c MailConfig) ToAddress() *mail.Address {
	m, _ := mail.ParseAddress(c.To)
	return m
}

// HTTPConfig contains elements of configuration
type HTTPConfig struct {
	Headers map[string]string `json:"headers"`
}

// Config contains elements of configuration
type Config struct {
	Mail MailConfig `json:"mail"`
	HTTP HTTPConfig `json:"http"`
}

var root Config

func Read(configFile string) {
	configContent, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(configContent, &root)
	if err != nil {
		log.Fatal(err)
	}
}

func HTTP() HTTPConfig {
	return root.HTTP
}

func Mail() MailConfig {
	return root.Mail
}

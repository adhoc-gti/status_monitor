package status

import (
	"sync"
	"time"

	"gitlab.com/adhoc-gti/status_monitor/server/test"
)

// Status handles shared result storage and analysis
type Status struct {
	results map[test.Test]*test.TestResultHistory
	mu      sync.RWMutex
}

// NewStatus creates a blank Status
func NewStatus() *Status {
	return &Status{map[test.Test]*test.TestResultHistory{}, sync.RWMutex{}}
}

// Of finds the latest result of a Test
func (s *Status) Of(t test.Test) (test.TestResult, bool) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	h, ok := s.results[t]
	if !ok {
		return test.TestResult{}, false
	}
	r := h.Last()
	r.Success = h.Success()
	return r, true
}

// Results lists all of the latest TestResults
func (s *Status) Results() []test.TestResult {
	s.mu.RLock()
	defer s.mu.RUnlock()
	var results = []test.TestResult{}
	for _, result := range s.results {
		r := result.Last()
		r.Success = result.Success()
		r.Warning = result.Warning()
		r.Disabled = !r.Test.Active(time.Now())
		results = append(results, r)
	}

	return results
}

// Update records the result of a test
func (s *Status) Update(result test.TestResult) {
	s.mu.Lock()
	defer s.mu.Unlock()
	h, ok := s.results[result.Test]
	if !ok {
		h = test.NewTestResultHistory(10)
		s.results[result.Test] = h
	}
	h.Append(result)
}

// IsUp will be true iff all tests are up
func (s *Status) IsUp() bool {
	s.mu.RLock()
	defer s.mu.RUnlock()
	ok := true
	for _, result := range s.results {
		ok = ok && result.Success()
	}

	return ok
}

// UpCount counts tests that are currently up
func (s *Status) UpCount() int {
	s.mu.RLock()
	defer s.mu.RUnlock()
	count := 0
	for _, result := range s.results {
		if result.Success() {
			count++
		}
	}

	return count
}

// Length gives the number of results
func (s *Status) Length() int {
	s.mu.RLock()
	defer s.mu.RUnlock()
	return len(s.results)
}

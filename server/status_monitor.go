package main

import (
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"

	"gitlab.com/adhoc-gti/status_monitor/server/api"
	"gitlab.com/adhoc-gti/status_monitor/server/config"
	"gitlab.com/adhoc-gti/status_monitor/server/processor"
	"gitlab.com/adhoc-gti/status_monitor/server/runner"
	"gitlab.com/adhoc-gti/status_monitor/server/scheduler"
	"gitlab.com/adhoc-gti/status_monitor/server/status"
	"gitlab.com/adhoc-gti/status_monitor/server/test"
	"gopkg.in/yaml.v2"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	tests := []test.Test{}

	var testsFile string
	var configFile string

	if len(os.Args) > 1 {
		testsFile = os.Args[1]
	} else {
		testsFile = "tests.yml"
	}

	if len(os.Args) > 2 {
		configFile = os.Args[2]
	} else {
		configFile = "config.yml"
	}

	testsContent, err := ioutil.ReadFile(testsFile)
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(testsContent, &tests)
	if err != nil {
		log.Fatal(err)
	}

	config.Read(configFile)

	status := status.NewStatus()

	queue := make(chan test.Test, 10)
	results := make(chan test.TestResult, 10)

	go scheduler.ScheduleTests(tests, queue)
	go runner.RunTests(queue, results)
	go processor.ProcessResults(status, results)

	api.Serve(status)
}

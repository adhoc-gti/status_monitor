package runner

import "gitlab.com/adhoc-gti/status_monitor/server/test"

// RunTests executes queued tests in a loop, dispatching the result
func RunTests(tests <-chan test.Test, results chan<- test.TestResult) {
	for t := range tests {
		go func(t test.Test) {
			results <- t.Run()
		}(t)
	}
}

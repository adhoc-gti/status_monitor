package processor

import (
	"log"
	"time"

	"gitlab.com/adhoc-gti/status_monitor/server/notification"
	"gitlab.com/adhoc-gti/status_monitor/server/test"
)

type Edge int

const (
	EDGE_DOWN Edge = iota
	EDGE_UP
)

// DetectEdge calls handler on change
func DetectEdge(old test.TestResult, new test.TestResult, handler func(test.TestResult, Edge)) {
	if old.Success && !new.Success {
		handler(new, EDGE_DOWN)
	}
	if !old.Success && new.Success {
		handler(new, EDGE_UP)
	}
}

// EdgeHandler performs notification actions
func EdgeHandler(result test.TestResult, edge Edge) {
	at := time.Now()
	switch edge {
	case EDGE_UP:
		log.Printf("++ %-6s %s\n", result.Test.Type, result.Test.Target)
		go notification.SendUpNotification(at, result)
	case EDGE_DOWN:
		log.Printf("-- %-6s %s\n", result.Test.Type, result.Test.Target)
		go notification.SendDownNotification(at, result)
	}
}

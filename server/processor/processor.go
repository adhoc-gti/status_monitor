package processor

import (
	"log"

	"gitlab.com/adhoc-gti/status_monitor/server/status"
	"gitlab.com/adhoc-gti/status_monitor/server/test"
)

// LogResult writes test result to log
func LogResult(result test.TestResult) {
	if result.Success {
		log.Printf("OK %-6s %s", result.Test.Type, result.Test.Target)
	} else {
		log.Printf("NO %-6s %s", result.Test.Type, result.Test.Target)
	}
}

// ProcessResults logs each test result, updates state and reschedules the test
func ProcessResults(status *status.Status, results <-chan test.TestResult) {
	for result := range results {
		// write result
		go LogResult(result)

		// update status, but track previous status
		previous, okPrevious := status.Of(result.Test)
		status.Update(result)
		current, okCurrent := status.Of(result.Test)

		// handle notifications
		if okPrevious && okCurrent {
			go DetectEdge(previous, current, EdgeHandler)
		}
	}
}

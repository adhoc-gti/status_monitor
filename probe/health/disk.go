package health

import (
	"github.com/alecthomas/units"
	"gitlab.com/adhoc-gti/status_monitor/disk"
)

// CheckFreeDiskSpace validates free space
func CheckFreeDiskSpace(path string, expected units.Base2Bytes) (string, bool) {
	check := "FreeDiskSpace"

	free, err := disk.FreeSpace(path)
	if err != nil {
		return check, false
	}

	return check, free > expected
}

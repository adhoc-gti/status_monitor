package health

import "github.com/alecthomas/units"

// Check runs health checks against the system
func Check(health *Health) {
	check, ok := CheckFreeDiskSpace("/", 5*units.Gibibyte)
	health.Update(check, ok)
}

package health

import "time"

// Monitor continuously runs health checks against the system
func Monitor(health *Health) {
	for {
		Check(health)
		time.Sleep(1000 * time.Millisecond)
	}
}

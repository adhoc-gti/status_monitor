package health

import "sync"

// Health summarizes system checks
type Health struct {
	results map[string]bool
	mu      sync.RWMutex
}

func New() Health {
	return Health{results: map[string]bool{}, mu: sync.RWMutex{}}
}

// Update records a health check status
func (h *Health) Update(check string, ok bool) {
	h.mu.Lock()
	defer h.mu.Unlock()
	h.results[check] = ok
}

// Results synthesizes a summary
func (h *Health) Results() map[string]bool {
	h.mu.RLock()
	defer h.mu.RUnlock()
	return h.results
}

// Ok synthesizes a minimal response
func (h *Health) Ok() bool {
	h.mu.RLock()
	defer h.mu.RUnlock()
	ok := true
	for _, v := range h.results {
		ok = v && ok
	}
	return ok
}

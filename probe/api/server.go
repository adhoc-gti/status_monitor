package api

import (
	"net/http"

	"gitlab.com/adhoc-gti/status_monitor/probe/health"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// Serve responds to HTTP requests
func Serve(h *health.Health) {
	router := mux.NewRouter()
	router.StrictSlash(false)

	api := router.PathPrefix("/api/").Subrouter()
	api.Path("/health").Methods("GET").HandlerFunc(func(rw http.ResponseWriter, r *http.Request) { HealthHandler(h, rw, r) })

	n := negroni.Classic()
	n.UseHandler(router)
	n.Run(":8421")
}

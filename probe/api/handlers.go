package api

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/adhoc-gti/status_monitor/probe/health"
)

// HealthHandler responds to health status requests
func HealthHandler(h *health.Health, rw http.ResponseWriter, r *http.Request) {
	results := h.Results()
	response, err := json.Marshal(results)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		log.Fatal(err)
	}

	rw.Header().Add("Content-Type", "application/json")
	if !h.Ok() {
		rw.WriteHeader(http.StatusServiceUnavailable)
	}
	_, err = rw.Write(response)
	if err != nil {
		log.Fatal(err)
	}
}

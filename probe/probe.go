package main

import (
	"gitlab.com/adhoc-gti/status_monitor/probe/api"
	"gitlab.com/adhoc-gti/status_monitor/probe/health"
)

func main() {
	h := health.New()
	go api.Serve(&h)
	health.Monitor(&h)
}

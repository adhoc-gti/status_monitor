package terminal

import (
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"
	"unsafe"
)

type winsize struct {
	Row    uint16
	Col    uint16
	Xpixel uint16
	Ypixel uint16
}

func GetWinsize() (*winsize, error) {
	ws := new(winsize)

	r1, _, errno := syscall.Syscall(syscall.SYS_IOCTL,
		uintptr(syscall.Stdin),
		uintptr(_TIOCGWINSZ),
		uintptr(unsafe.Pointer(ws)),
	)

	if int(r1) == -1 {
		return nil, os.NewSyscallError("GetWinsize", errno)
	}
	return ws, nil
}

func Cols() int {
	ws, _ := GetWinsize()
	return int(ws.Col)

	var s string

	if s = os.Getenv("COLUMNS"); s == "" {
		cmd := exec.Command("tput", "cols")
		b, err := cmd.Output()
		if err != nil {
			panic(err)
		}
		s = strings.TrimSpace(string(b))
	}

	if n, err := strconv.ParseInt(s, 10, 0); err == nil {
		return int(n)
	}

	return 0
}

func Lines() int {
	ws, _ := GetWinsize()
	return int(ws.Row)

	var s string

	if s = os.Getenv("LINES"); s == "" {
		cmd := exec.Command("tput", "lines")
		b, err := cmd.Output()
		if err != nil {
			panic(err)
		}
		s = strings.TrimSpace(string(b))
	}

	if n, err := strconv.ParseInt(s, 10, 0); err == nil {
		return int(n)
	}

	return 0
}

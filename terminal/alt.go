package terminal

import (
	"fmt"
	"os"
)

func Alt() {
	fmt.Fprintf(os.Stdout, "\033[?1049h") // smcup
}

func Unalt() {
	fmt.Fprintf(os.Stdout, "\033[?1049l") // rmcup
}

func ClearScreen() {
	fmt.Fprintf(os.Stdout, "\033[2J") // clear all screen
}

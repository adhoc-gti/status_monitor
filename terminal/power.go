package terminal

import (
	"fmt"
	"os"
)

func Caffeinate() {
	fmt.Fprintf(os.Stdout, "\033[14;0]") // powerdown
	fmt.Fprintf(os.Stdout, "\033[9;0]")  // blank
}
